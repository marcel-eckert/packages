# Changelog

## 2020/09/30 - IEEE Std. 1076-2019

* Updated source files with Apache License, 2.0 license header.  
  No VHDL code changes.

## 2019/07/11 - Initial upload of VHDL package files

* Finalized VHDL packages handed over from P1076 working group for balloting.
